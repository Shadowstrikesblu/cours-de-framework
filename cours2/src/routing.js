import { createRouter, createWebHistory } from "vue-router";
// Ici, on importe les composants qu'on veut configurer
// Dans ce cas, ce sera Accueil et Calculatrice
import TP from "./components/TP1.vue";
import Home from "./App.vue";

// Nous créons d'abord nos Routes
const routes = [

  {
    path: "/",
    component: Home,
  },

  {
    path: "/TP1",
    component: TP,
  },
];

export default createRouter({

  history: createWebHistory(),

  routes,
});