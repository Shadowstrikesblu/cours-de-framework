import { createStore } from 'vuex';
const store = createStore({
    state(){
      return {
        counter : 0,
      }
    },
    getters: {
      getCounter: (state)=>state.counter,
    },
    mutations: {
      setCounter(state,payload){
        state.counter = payload;
      }
    }
  });

export default createStore({
    state: {
        products: [
          {
            id: 1,
            name: "Chelsea Shoes",
            price: 200,
            shortdesc: "Best Drip in the Market",
            url: "images/chelsea-shoes.png"
          }
        ]
},
})